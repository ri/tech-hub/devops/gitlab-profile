## Overview

DevOps group is a place for the EBRAINS devOps team to collaborate, prioritize work to be done, document important procedures and plan future development and operations processes. This group contains multiple groups, each focusing on separate operations actions and processes. 

## Purpose

The purpose of this group is to manage the day-to-day deployment lifecycle, define Infrastructure as Code via Ansible and Terraform scripts and collaborate/ coordinate word via specific gitlab issue boards.   

## Structure

1. /DevOps/Ansible: This Group contains Ansible roles that can be reused across defferent playbooks. Each Ansible role is a separate project inside Ansible Group for easier maintanance.  
2. /DevOps/Apps: This Group contains Infrastructure as Code (Terraform recipes) for VM creation for EBRAINS science services organised by DevOps team. Each EBRAINS science service is organised in separate project inside Apps Group.
3. /DevOps/Platform: This Group contains Infrastructure as Code (Terraform recipes) for VM creation for EBRAINS platform services organised by DevOps team.  Each EBRAINS platform service is organised in separate project inside Platform Group.  
4. /DevOps/migration-base: This group contains milestones, day-to-day migration work, and is used for coordination and prioritization of migration efforts. For more information see [here](https://gitlab.ebrains.eu/ri/tech-hub/devops/migration) 
5. /DevOps/devops-base: This group contains day-to-day operations work for team coordination as gitlab [issues](https://gitlab.ebrains.eu/ri/tech-hub/devops/base/-/issues/?sort=updated_desc&state=opened&first_page_size=100)
6. /DevOps/Docs: This group contains comprehensive guide for documenting and sharing information about the DevOps practices and tools implemented within our organization.
7. /DevOps/helm-charts:

Disclaimer: Projects located in each of the abovementioned groups contain master branches that are protected by default. This means that developers can not push code directly to these branches. If developers needed to contibute to these projects,  they can create a new branch, push code into the new branch, and make a merge request.

## Ownership

The owner of DevOps group is:

[Sofia (ATHENA)](mailto:s.karvounari@athenarc.gr)

Onwres and maintainers of the projects inside each group are responsible for overseeing the content, moderation, and activities within the repository.

## Membership Guidelines

All members of the EBRAINS devOps team must be members of the group and they must be directly added by the owners of DevOps group.


## Contacts

| Contact points |  
|-----------------------|
| [Sofia (ATHENA)](mailto:s.karvounari@athenarc.gr)  | 
| [Michalis (ATHENA)](mailto:alexakis@athenarc.gr)  | 
| [Nikos P. (ATHENA)](mailto:npappas@athenarc.gr)  |
